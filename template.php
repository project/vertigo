<?php

function vertigo_regions(){

  $my_regions = array(

    'footerleft' => t('Footer Left'),
    'footermiddle1' => t('Footer Middle 1'),
    'footermiddle2' => t('Footer Middle 2'),
    'footerright' => t('Footer Right')
  );

  $result = array_merge($my_regions, phptemplate_regions());
  return $result;
}
?>
