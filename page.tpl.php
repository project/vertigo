<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<meta name="distribution" content="global" />
<meta name="robots" content="follow, all" />
<meta name="language" content="en, sv" />

  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <!-- leave this for stats please -->

</head>

<body <?php print theme("onload_attribute"); ?>>
<div id="navbar">
  <div id="navbarright">
      <?php if (is_array($primary_links)) : ?>
        <?php 
          foreach($primary_links AS $links){
            echo l($links['title'], $links['href']) .' | ';
          }
          endif;
        ?>
  </div>
</div>

<!-- The main column ends  -->

<div id="content">

  <div id="contentleft">
    <?php if ($title != ""): ?>
      <h2 class="content-title"><?php print $title ?></h2>
    <?php endif; ?>
    <?php if ($tabs != ""): ?>
      <?php print $tabs ?>
    <?php endif; ?>
          
    <?php if ($mission != ""): ?>
      <p id="mission"><?php print $mission ?></p>
    <?php endif; ?>
          
    <?php if ($help != ""): ?>
      <p id="help"><?php print $help ?></p>
    <?php endif; ?>
          
    <?php if ($messages != ""): ?>
      <div id="message"><?php print $messages ?></div>
    <?php endif; ?>
    
    <!-- start main content -->
    <?php print($content) ?>
    <!-- end main content -->
      
  </div>
	
  <?php if ($sidebar_left != ""): ?>
  <div id="l_sidebar">
      <?php print $sidebar_left ?>
  </div>
  <?php endif; ?>  
  
  <?php if ($sidebar_right != ""): ?>
  <div id="r_sidebar">
      <?php print $sidebar_right ?>
  </div>
    <?php endif; ?>  
</div>

<!-- The main column ends  -->
<!-- begin footer -->

<div style="clear:both;"></div>
<div style="clear:both;"></div>

<div id="footerbg">
  <div id="footer">
    
    <div id="footerleft">
      <?php print $footerleft; ?>
    </div>
    
    <div id="footermiddle1">
      <?php print $footermiddle1; ?>
    </div>
    
    <div id="footermiddle2">
      <?php print $footermiddle2; ?>
    </div>
  
    <div id="footerright">
      <?php print $footerright; ?>
    </div>
    
    <div style="clear: both;">
      &nbsp;
    </div>
  </div>
</div>

<?php print $closure;?>
  </body>
</html>

