Vertigo Drupal Theme - Derived from Vertigo Wordpress Theme

Drupal Version 5.x compatible.

Theme conversion by Agileware Pty Ltd. Australia

Tarrant Marshall
http://www.agileware.net/

Original Design: 

Vertigo Drupal Theme
http://www.briangardner.com/themes/vertigo-electrified-wordpress-theme.htm
This theme is under GPL License, http://www.gnu.org/licenses/gpl-2.0.txt

Brian Gardner
bmg1227@hotmail.com
http://www.briangardner.com


----------------
  Installation
----------------

INSTALL: 
1. Unpack this archive into your drupal/themes directory.
2. Go to Administration -> Site Building -> Themes
3. Select Vertigo theme
